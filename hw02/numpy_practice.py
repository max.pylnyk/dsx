#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
iris = np.genfromtxt(url, delimiter=',', dtype='object')
names = ('sepallength', 'sepalwidth', 'petallength', 'petalwidth', 'species')


# ### 1

# In[2]:


col_count = 4
species = iris[:, col_count]

print('#1')
print(species)
print()


# ### 2

# In[77]:


features = iris[:, :col_count].astype('float')

print('#2')
print(features)
print()


# ### 3

# In[11]:


stat_col = 0
sepal_len = features[:, stat_col]

mean = np.mean(sepal_len)
median = np.median(sepal_len)
std = np.std(sepal_len)

print('#3')
print(f'mean: {mean:.2f}')
print(f'median: {median:.2f}')
print(f'std: {std:.2f}')
print()


# ### 4

# In[12]:


nan_count = 20
rnd_indices = np.random.choice(
    features.size, nan_count, replace=False)
features.ravel().put(rnd_indices, np.nan)

print('#4')
print(features)
print()


# ### 5

# In[35]:


search_col = 0
nan_indices = np.where(
    np.isnan(features[:, search_col])
    )[0]

print('#5')
print(nan_indices)
print()


# ### 6

# In[41]:


filtered = features[
    (features[:, 2] > 1.5) & (features[:, 0] < 5)]

print('#6')
print(filtered)
print()


# ### 7

# In[57]:


features[np.isnan(features)] = 0

print('#7')
print(features)
print()


# ### 8

# In[79]:


unique, u_counts = np.unique(features, return_counts=True)

print('#8')
print('unique elements:', unique)
print()
print('unique counts:', u_counts)
print()


# ### 9

# In[52]:


feat, ures = np.array_split(features, 2)

print('#9')
print(feat)
print()
print(ures)
print()


# ### 10

# In[54]:


sort_col = 0
sort_indices = feat[:, sort_col].argsort()
feat = feat[sort_indices]

sort_indices = ures[:, sort_col].argsort()
ures = ures[sort_indices[::-1]]

print('#10')
print(feat)
print()
print(ures)
print()


# ### 11

# In[81]:


features = np.vstack([feat, ures])

print('#11')
print(features)
print()


# ### 12

# In[80]:


most_often = unique[np.argmax(u_counts)]

print('#12')
print('most often value:', most_often)
print()


# ### 13

# In[78]:


def spreader(value, median):
    if value < median:
        return value * 2
    return value / 4
        
def col_func(arr):
    median = np.median(arr)
    v_spreader = np.vectorize(spreader, otypes=[float])
    return v_spreader(arr, median)

spread_col = 2

print('#13')
print(features[:, spread_col])
print()

np.copyto(features[:, spread_col],
    col_func(features[:, spread_col]))

print(features[:, spread_col])

