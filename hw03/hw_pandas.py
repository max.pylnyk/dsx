#!/usr/bin/env python
# coding: utf-8

# <center>
# <img src="../../img/ods_stickers.jpg">
# ## Открытый курс по машинному обучению
# <center>
# Автор материала: Юрий Кашницкий, программист-исследователь Mail.Ru Group <br> 
# 
# Материал распространяется на условиях лицензии [Creative Commons CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/). Можно использовать в любых целях (редактировать, поправлять и брать за основу), кроме коммерческих, но с обязательным упоминанием автора материала.

# # <center>Домашнее задание № 1 (демо).<br> Анализ данных по доходу населения UCI Adult</center>

# **В задании предлагается с помощью Pandas ответить на несколько вопросов по данным репозитория UCI [Adult](https://archive.ics.uci.edu/ml/datasets/Adult) (качать данные не надо – они уже есть в репозитории).**

# Уникальные значения признаков (больше информации по ссылке выше):
# - age: continuous.
# - workclass: Private, Self-emp-not-inc, Self-emp-inc, Federal-gov, Local-gov, State-gov, Without-pay, Never-worked.
# - fnlwgt: continuous.
# - education: Bachelors, Some-college, 11th, HS-grad, Prof-school, Assoc-acdm, Assoc-voc, 9th, 7th-8th, 12th, Masters, 1st-4th, 10th, Doctorate, 5th-6th, Preschool.
# - education-num: continuous.
# - marital-status: Married-civ-spouse, Divorced, Never-married, Separated, Widowed, Married-spouse-absent, Married-AF-spouse.
# - occupation: Tech-support, Craft-repair, Other-service, Sales, Exec-managerial, Prof-specialty, Handlers-cleaners, Machine-op-inspct, Adm-clerical, Farming-fishing, Transport-moving, Priv-house-serv, Protective-serv, Armed-Forces.
# - relationship: Wife, Own-child, Husband, Not-in-family, Other-relative, Unmarried.
# - race: White, Asian-Pac-Islander, Amer-Indian-Eskimo, Other, Black.
# - sex: Female, Male.
# - capital-gain: continuous.
# - capital-loss: continuous.
# - hours-per-week: continuous.
# - native-country: United-States, Cambodia, England, Puerto-Rico, Canada, Germany, Outlying-US(Guam-USVI-etc), India, Japan, Greece, South, China, Cuba, Iran, Honduras, Philippines, Italy, Poland, Jamaica, Vietnam, Mexico, Portugal, Ireland, France, Dominican-Republic, Laos, Ecuador, Taiwan, Haiti, Columbia, Hungary, Guatemala, Nicaragua, Scotland, Thailand, Yugoslavia, El-Salvador, Trinadad&Tobago, Peru, Hong, Holand-Netherlands.   
# - salary: >50K,<=50K

# In[31]:


import numpy as np
import pandas as pd


# In[12]:


data = pd.read_csv('adult.data.csv')
#data.head()


# **1. Сколько мужчин и женщин (признак *sex*) представлено в этом наборе данных?**

# In[151]:


print('#1')
print(data['sex'].value_counts())


# **2. Каков средний возраст (признак *age*) женщин?**

# In[150]:


avg_fem_age = data[data['sex'] == 'Female']['age'].mean()

print()
print('#2')
print(f"average female age: {avg_fem_age:.2f}")


# **3. Какова доля граждан Германии (признак *native-country*)?**

# In[152]:


germans_pct = data['native-country'].value_counts(normalize=True)['Germany'] * 100

print()
print('#3')
print(f"{germans_pct:.2f}% germans")


# **4-5. Каковы средние значения и среднеквадратичные отклонения возраста тех, кто получает более 50K в год (признак *salary*) и тех, кто получает менее 50K в год? **

# In[155]:


cols_to_show = ['age']
age_to_salary = data.groupby(
    ['salary'])[cols_to_show].agg([np.mean, np.std])

print()
print('#4-5')
print(age_to_salary)


# **6. Правда ли, что люди, которые получают больше 50k, имеют как минимум высшее образование? (признак *education – Bachelors, Prof-school, Assoc-acdm, Assoc-voc, Masters* или *Doctorate*)**

# In[154]:


higher_education = ['Bachelors', 'Prof-school',
    'Assoc-acdm', 'Assoc-voc', 'Masters', 'Doctorate']
edu_gets_money = data[data['salary'] == '>50K']['education'].isin(
    higher_education).all()

print()
print('#6')
print('education gets money:', edu_gets_money)


# **7. Выведите статистику возраста для каждой расы (признак *race*) и каждого пола. Используйте *groupby* и *describe*. Найдите таким образом максимальный возраст мужчин расы *Amer-Indian-Eskimo*.**

# In[156]:


age_to_race = data.groupby(
    ['race', 'sex'])[cols_to_show].describe(percentiles=[0.5])

print()
print('#7')
print(age_to_race)
print()
print('max male amer-indian-eskimo age:',
    age_to_race.loc[('Amer-Indian-Eskimo', 'Male'), ('age', 'max')])


# **8. Среди кого больше доля зарабатывающих много (>50K): среди женатых или холостых мужчин (признак *marital-status*)? Женатыми считаем тех, у кого *marital-status* начинается с *Married* (Married-civ-spouse, Married-spouse-absent или Married-AF-spouse), остальных считаем холостыми.**

# In[157]:


marriage = ['Married-civ-spouse', 'Married-spouse-absent', 'Married-AF-spouse']
married_men = data[(data['salary'] == '>50K') & (data['sex'] == 'Male')
    ]['marital-status'].isin(marriage).value_counts()

print()
print('#8')
print('married men earn more:', married_men[True] > married_men[False])


# **9. Какое максимальное число часов человек работает в неделю (признак *hours-per-week*)? Сколько людей работают такое количество часов и каков среди них процент зарабатывающих много?**

# In[158]:


max_hours = data['hours-per-week'].max()
hard_workers = data[data['hours-per-week'] == max_hours]
salaries = hard_workers['salary'].value_counts(normalize=True)

print()
print('#9')
print(f'{hard_workers.shape[0]} people works {max_hours} hours per week')
print(f'{100*salaries[">50K"]:.1f}% of them earn >50K')


# **10. Посчитайте среднее время работы (*hours-per-week*) зарабатывающих мало и много (*salary*) для каждой страны (*native-country*).**

# In[159]:


cols_to_show = ['hours-per-week']
hours_to_countries = data.groupby(
    ['native-country', 'salary'])[cols_to_show].agg([np.mean])

print()
print('#10')
print(hours_to_countries)

